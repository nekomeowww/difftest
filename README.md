# difftest

## Installation

```shell
go install gitlab.com/studentmain/difftest/difftest@latest
```

## Usage

Bash:

```bash
go test `difftest`
```

Powershell:

```powershell
go test (difftest)
```

### Supported parameters

All following parameters are optional.

```shell
difftest -directory ./repo -oldrev HEAD~123 -newrev HEAD -skips txt,md,docx,mod,sum -verbose
```
