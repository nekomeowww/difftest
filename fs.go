package difftest

import (
	"errors"
	"log"
	"os"
	"path"
	"strings"

	"github.com/samber/lo"
	"golang.org/x/mod/modfile"
)

type packageSpec struct {
	PkgPath string
	FSPath  string
	Imports []*packageSpec

	changed  bool
	importby []*packageSpec
}

func (p *packageSpec) markImporter() {
	for _, p2 := range p.importby {
		// either already marked by dfs
		// or manually marked and not scanned
		// all package will scan by for range loop
		if p2.changed {
			continue
		}
		p2.changed = true
		p2.markImporter()
	}
}

func (p *packageSpec) resolveDependency(f *FS) {
	for n, v := range p.Imports {
		if pkg, ok := f.byPkgPath[v.PkgPath]; ok {
			if pkg != v {
				p.Imports[n] = pkg
			}
		}
	}
}

type FS struct {
	module    string
	root      string
	byFSPath  map[string][]*packageSpec
	byPkgPath map[string]*packageSpec

	updated bool
}

func (f *FS) add(p *packageSpec) {
	if _, exist := f.byPkgPath[p.PkgPath]; exist {
		log.Panicln("duplicate package", p.PkgPath)
	}
	f.byPkgPath[p.PkgPath] = p
	if p.FSPath == "" {
		return
	}
	if _, exist := f.byFSPath[p.FSPath]; !exist {
		f.byFSPath[p.FSPath] = []*packageSpec{}
	}
	if !lo.Contains(f.byFSPath[p.FSPath], p) {
		f.byFSPath[p.FSPath] = append(f.byFSPath[p.FSPath], p)
	}

	f.updated = false
}

func (f *FS) addDependency(path string) {
	f.add(&packageSpec{PkgPath: path, Imports: []*packageSpec{}})
}

func (f *FS) addPackage(fspath, pkg string, dependency []string) {
	maybeRoot := fspath[:len(f.root)]
	if maybeRoot != f.root {
		log.Panicln("outside of root dir", maybeRoot)
	}

	// fspath->pkgpath
	pathdiff := fspath[len(f.root):]
	pkgPath := ""
	replaceLast := func(path, pkg string) string {
		pathSect := strings.Split(path, "/")
		lastSect := lo.Must(lo.Last(pathSect))
		switch pkg {
		case lastSect:
		case lastSect + "_test":
			pathSect[len(pathSect)-1] = pkg
		case "main":
			pathSect = append(pathSect, pkg)
		default:
			log.Panicln("unexpected package name", pkg, fspath)
		}
		return strings.Join(pathSect, "/")
	}
	if len(pathdiff) == 0 {
		// at root dir
		pkgPath = replaceLast(f.module, pkg)
	} else {
		pkgPath = path.Join(f.module, replaceLast(pathdiff, pkg))
	}

	// dependency->packagespec
	dep := lo.Map(dependency, func(d string, _ int) *packageSpec { return &packageSpec{PkgPath: d} })
	p := &packageSpec{
		PkgPath: pkgPath,
		FSPath:  fspath,
		Imports: dep,
	}
	f.add(p)
}

func (f *FS) MarkChangedFile(rpath string) {
	f.updated = false

	fpath := path.Join(f.root, rpath)
	fdir := path.Dir(fpath)
	if _, ok := f.byFSPath[fdir]; !ok {
		log.Println("unindexed path", fdir)
		found := false
		for fdir = path.Dir(fdir); fdir != "/" && fdir != "."; fdir = path.Dir(fdir) {
			if _, ok := f.byFSPath[fdir]; ok {
				found = true
				break
			}
		}
		if !found {
			log.Println("parent path are unindexed too")
		} else {
			log.Println("use indexed parent path instead", fdir)
		}
	}

	// mark all pkg under fspath
	pkgs, ok := f.byFSPath[fdir]
	if !ok {
		log.Panicln("can't get package", rpath)
	}
	for _, pkg := range pkgs {
		pkg.changed = true
	}
}

func (f *FS) MarkChangedPackage(pkgpath string) {
	if pkg, ok := f.byPkgPath[pkgpath]; ok {
		f.updated = false
		pkg.changed = true
	} else {
		log.Println("changed package is not in dependency graph", pkgpath)
	}
}

func (f *FS) updateDependencyGraph() {
	f.updated = false
	for path, pkg := range f.byPkgPath {
		pkg.resolveDependency(f)
		if pkg.PkgPath != path {
			log.Panicln("package path mismatch", path, pkg.PkgPath)
		}

		if pkg.FSPath != "" {
			if _, ok := f.byFSPath[pkg.FSPath]; !ok {
				f.byFSPath[pkg.FSPath] = []*packageSpec{}
			}
			if !lo.Contains(f.byFSPath[pkg.FSPath], pkg) {
				f.byFSPath[pkg.FSPath] = append(f.byFSPath[pkg.FSPath], pkg)
			}
		}

		for _, i := range pkg.Imports {
			if i.importby == nil {
				i.importby = []*packageSpec{}
			}

			if !lo.Contains(i.importby, pkg) {
				i.importby = append(i.importby, pkg)
			}
		}
	}
}

func (f *FS) updateChanged() {
	f.updateDependencyGraph()
	for _, pkg := range f.byPkgPath {
		if !pkg.changed {
			continue
		}
		pkg.markImporter()
	}

	f.updated = true
}

func (f *FS) GetChanged() []string {
	if !f.updated {
		f.updateChanged()
	}

	return lo.FilterMap(lo.Values(f.byPkgPath), func(p *packageSpec, _ int) (string, bool) { return p.PkgPath, p.changed })
}

func (f *FS) GetAll() []string {
	return lo.Uniq(lo.Map(
		lo.Flatten(lo.Values(f.byFSPath)),
		func(p *packageSpec, _ int) string { return p.PkgPath },
	))
}

func (f *FS) scanDir() {
	cwd := lo.Must(os.Getwd())
	defer lo.Must0(os.Chdir(cwd))

	gomod, err := os.ReadFile("go.mod")
	if err == nil {
		if cwd == f.root {
			m, err := modfile.Parse("", gomod, nil)
			if err != nil {
				log.Panic(err)
			}
			f.module = m.Module.Mod.Path

			for _, r := range m.Require {
				if r.Indirect {
					continue
				}
				f.addDependency(r.Mod.Path)
			}

		} else {
			log.Println("nested module is not supported", cwd)
			return
		}
	} else if !errors.Is(err, os.ErrNotExist) {
		log.Panicln(err)
	} else if cwd == f.root {
		log.Panicln("root go.mod not exist", cwd)
	}

	p := ParsePackageImport()
	for name, imports := range p {
		f.addPackage(cwd, name, imports)
	}

	ls := lo.Must(os.ReadDir(cwd))
	for _, fd := range ls {
		if !fd.IsDir() {
			continue
		}
		lo.Must0(os.Chdir(fd.Name()))
		f.scanDir()
		lo.Must0(os.Chdir(cwd))
	}
}

func (f *FS) FilterTestablePackage(pkgs []string) []string {
	return lo.Filter(pkgs, func(p string, _ int) bool {
		b := path.Base(p)
		if strings.HasSuffix(b, "_test") {
			return false
		}
		if b == "main" {
			return false
		}
		if !strings.HasPrefix(p, f.module) {
			return false
		}
		return true
	})
}

func NewFS(path string) *FS {
	cwd := lo.Must(os.Getwd())
	defer lo.Must0(os.Chdir(cwd))

	f := &FS{
		root:      path,
		byFSPath:  map[string][]*packageSpec{},
		byPkgPath: map[string]*packageSpec{},
	}
	lo.Must0(os.Chdir(f.root))
	f.scanDir()

	return f
}
