package difftest

import (
	"log"
	"path"
	"strings"

	"github.com/samber/lo"
)

func GetChangedPackage(dir, oldrev, newrev string, skipext []string) []string {
	log.Println("read repo at", dir)
	fs := NewFS(dir)
	diffpair := NewDiffPair(dir, oldrev, newrev)
	pdiff, fulltest := diffpair.DiffModfile()

	skipext = lo.Map(skipext, func(s string, _ int) string {
		if s == "" {
			return ""
		}
		if strings.HasPrefix(s, ".") {
			return s
		}
		return "." + s
	})

	var changed []string
	if fulltest {
		log.Println("need full test")
		changed = fs.GetAll()
	} else {
		fdiff := diffpair.Diff()
		_ = fulltest
		for _, c := range pdiff {
			fs.MarkChangedPackage(c)
		}
		for _, n := range fdiff {
			ext := path.Ext(n)
			if lo.Contains(skipext, ext) {
				continue
			}
			fs.MarkChangedFile(n)
		}
		changed = fs.GetChanged()
	}
	return fs.FilterTestablePackage(changed)
}
