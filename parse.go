package difftest

import (
	"go/ast"
	"go/parser"
	"go/token"
	"log"
	"os"
	"strings"

	"github.com/samber/lo"
	"golang.org/x/mod/modfile"
)

func ParsePackageImport() map[string][]string {
	fset := token.NewFileSet()
	p, err := parser.ParseDir(fset, lo.Must(os.Getwd()), nil, parser.ImportsOnly)
	if err != nil {
		log.Println("can't parse directory", err)
	}
	imports := map[string][]string{}
	for modname, modAst := range p {
		i1 := lo.FlatMap(lo.Values(modAst.Files), func(a *ast.File, _ int) []string {
			return lo.Map(a.Imports, func(i *ast.ImportSpec, _ int) string {
				return strings.Trim(i.Path.Value, "\"")
			})
		})

		i1 = lo.Uniq(i1)
		// skip stdlib
		i1 = lo.Filter(i1, func(s string, _ int) bool {
			path := strings.Split(s, "/")
			return strings.Contains(path[0], ".")
		})

		imports[modname] = lo.Uniq(i1)
	}
	return imports
}

func ParseModfileStr(b string) *modfile.File {
	return lo.Must(modfile.Parse("", []byte(b), nil))
}
