package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/samber/lo"
	"gitlab.com/studentmain/difftest"
)

func main() {
	dir := flag.String("directory", "", "project directory")
	newrev := flag.String("newrev", "HEAD", "")
	oldrev := flag.String("oldrev", "HEAD~1", "diff start point")
	verbose := flag.Bool("verbose", false, "enable logging")
	skips := flag.String("skipext", "txt,md,html,mod,sum", "skipped file extensions")
	flag.Parse()

	if !*verbose {
		log.SetOutput(io.Discard)
	}
	if *dir == "" {
		*dir = lo.Must(os.Getwd())
	}
	*dir = lo.Must(filepath.Abs(*dir))

	cantest := difftest.GetChangedPackage(*dir, *oldrev, *newrev, strings.Split(*skips, ","))
	if len(cantest) == 0 {
		log.Println("nothing to test")
		os.Exit(0)
	}
	for _, pkg := range cantest {
		fmt.Println(pkg)
	}
}
