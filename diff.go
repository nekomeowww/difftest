package difftest

import (
	"log"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/samber/lo"
	"golang.org/x/mod/modfile"
)

type DiffPair struct {
	Old *object.Tree
	New *object.Tree
}

func NewDiffPair(path, oldrev, newrev string) *DiffPair {
	repo := lo.Must(git.PlainOpen(path))
	tree1 := rev2tree(repo, oldrev)
	tree2 := rev2tree(repo, newrev)

	return &DiffPair{
		Old: tree1,
		New: tree2,
	}
}

func (d *DiffPair) Diff() []string {
	diff := lo.Must(d.New.Diff(d.Old))
	diffstr := lo.FilterMap(diff, func(o *object.Change, i int) (string, bool) {
		return o.From.Name, len(o.From.Name) != 0
	})
	return diffstr
}

func (d *DiffPair) DiffModfile() (changedPackages []string, fundamentalChange bool) {
	newModfile, err := d.New.File("go.mod")
	if err != nil {
		return nil, true
	}

	oldModfile, err := d.Old.File("go.mod")
	if err != nil {
		return nil, true
	}

	newm := ParseModfileStr(lo.Must(newModfile.Contents()))
	oldm := ParseModfileStr(lo.Must(oldModfile.Contents()))
	if oldm.Go.Version != newm.Go.Version {
		return nil, true
	}

	// really?
	if oldm.Module.Mod.Path != newm.Module.Mod.Path {
		return nil, true
	}

	// have same exclude list
	oldExcludeM := toMap(oldm.Exclude, func(m *modfile.Exclude) string { return m.Mod.Path })
	for _, exclude := range newm.Exclude {
		excludePath := exclude.Mod.Path
		if oldExclude, ok := oldExcludeM[excludePath]; ok {
			if oldExclude.Mod != exclude.Mod {
				return nil, true
			}
		} else {
			return nil, true
		}
	}

	// ... same retract list
	oldRetractM := toMap(oldm.Retract, func(m *modfile.Retract) string { return m.Rationale })
	for _, retract := range newm.Retract {
		retractRationale := retract.Rationale
		if oldRetract, ok := oldRetractM[retractRationale]; ok {
			if oldRetract.VersionInterval != retract.VersionInterval {
				return nil, true
			}
		} else {
			return nil, true
		}
	}

	// ...same replace list... finally
	oldReplaceM := toMap(oldm.Replace, func(m *modfile.Replace) string { return m.New.Path })
	for _, replace := range newm.Replace {
		replacePath := replace.New.Path
		if oldReplace, ok := oldReplaceM[replacePath]; ok {
			if oldReplace.New != replace.New || oldReplace.Old != replace.Old {
				return nil, true
			}
		} else {
			return nil, true
		}
	}

	// actual package diff
	diffPkg := []string{}
	oldRequireM := toMap(oldm.Require, func(m *modfile.Require) string { return m.Mod.Path })
	for _, require := range newm.Require {
		requirePath := require.Mod.Path
		if oldRequire, ok := oldRequireM[requirePath]; ok {
			if oldRequire.Mod.Version != require.Mod.Version {
				diffPkg = append(diffPkg, requirePath)
			}
		} else {
			diffPkg = append(diffPkg, requirePath)
		}
	}

	return diffPkg, false
}

func rev2tree(repo *git.Repository, revision string) *object.Tree {
	rev := lo.Must(repo.ResolveRevision(plumbing.Revision(revision)))
	commit := lo.Must(repo.CommitObject(*rev))
	return lo.Must(commit.Tree())
}

func toMap[T any, K comparable](t []T, f func(T) K) map[K]T {
	g := lo.GroupBy(t, f)
	return lo.MapValues(g, func(v []T, k K) T {
		if len(v) != 1 {
			log.Panic("key not unique or not exist", k)
		}
		return v[0]
	})
}
